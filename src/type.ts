export interface IUser {
  id: number;
  title: string;
}

export interface IPost {
  id: number;
  title: string;
}
