import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import Profile from "../pages/Profile";
import Posts from "../pages/Posts";
import Users from "../pages/Users";
import Home from "../pages/Home";
import NotFound from "../pages/NotFound";
import User from "../components/User";
import Post from "../components/Post";

import { IPost, IUser } from "../type";

type RoutesProps = {
  isAuth: boolean;
  changeIsAuth: () => void;
  users: IUser[];
  posts: IPost[];
};

const Routes = ({ isAuth, changeIsAuth, posts, users }: RoutesProps) => {
  return (
    <Switch>
      <Route path="/profile">
        {isAuth ? <Profile /> : <Redirect to="/" />}
      </Route>
      <Route path="/posts/:id">
        <Post posts={posts} />
      </Route>
      <Route path="/posts">
        <Posts posts={posts} />
      </Route>
      <Route path="/users/:id">
        <User users={users} />
      </Route>
      <Route path="/users">
        <Users users={users} />
      </Route>
      <Route path="/" exact>
        <Home isAuth={isAuth} changeIsAuth={changeIsAuth} />
      </Route>
      <Route>
        <NotFound />
      </Route>
    </Switch>
  );
};

export default Routes;
