import React from "react";
import { useHistory, useParams } from "react-router-dom";
import { IUser } from "../type";

type UserProps = {
  users: IUser[];
};

const User = ({ users }: UserProps) => {
  const history = useHistory();
  const back = () => {
    history.goBack();
  };
  const { id } = useParams<{ id: string }>();
  const user: IUser = users.filter((el) => ":" + el.id === id.toString())[0];
  return (
    <div className="user">
      <button onClick={back}>Go back</button>
      <h3>First name: {user.title}</h3>
      <h3>Last name: Ivanov</h3>
    </div>
  );
};

export default User;
