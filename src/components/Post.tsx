import React from "react";
import { useHistory, useParams } from "react-router-dom";
import { IPost } from "../type";

interface PostProps {
  posts: IPost[];
}

const Post = ({ posts }: PostProps) => {
  const history = useHistory();
  const back = () => {
    history.goBack();
  };
  const { id } = useParams<{ id: string }>();
  const post: IPost = posts.filter((el) => ":" + el.id === id.toString())[0];
  return (
    <div>
      <button onClick={back}>Go back</button>
      <h3>Detail page {post.title}</h3>
    </div>
  );
};

export default Post;
