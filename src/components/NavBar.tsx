import React from "react";
import { NavLink } from "react-router-dom";

const NavBar = () => {
  return (
    <div>
      <NavLink to="/profile">Profile</NavLink>
      <NavLink to="/posts">Posts</NavLink>
      <NavLink to="/users">Users</NavLink>
    </div>
  );
};

export default NavBar;
