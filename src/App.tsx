import React, { useState } from "react";
import "./App.css";
import NavBar from "./components/NavBar";
import Routes from "./routes/Routes";

import { IPost, IUser } from "./type";

const posts: IPost[] = [
  { id: 1, title: "News" },
  { id: 2, title: "Sport" },
  { id: 3, title: "Auto" },
  { id: 4, title: "Finance" },
];

const users: IUser[] = [
  { id: 1, title: "John" },
  { id: 2, title: "Maks" },
  { id: 3, title: "Fedor" },
  { id: 4, title: "Mark" },
  { id: 5, title: "Ivan" },
];
function App() {
  const [isAuth, setIsAuth] = useState(false);
  const changeIsAuth = () => setIsAuth(!isAuth);

  return (
    <div className="App">
      <NavBar />
      <Routes
        isAuth={isAuth}
        changeIsAuth={changeIsAuth}
        posts={posts}
        users={users}
      />
    </div>
  );
}

export default App;
