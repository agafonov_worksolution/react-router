import React from "react";
import { NavLink } from "react-router-dom";

const NotFound = () => {
  return (
    <div>
      <h1>Page not found</h1>
      <NavLink to="/">Home</NavLink>
    </div>
  );
};
export default NotFound;
