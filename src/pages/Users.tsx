import React from "react";
import { NavLink } from "react-router-dom";
import { IUser } from "../type";

type UsersProps = {
  users: IUser[];
};

const Users = ({ users }: UsersProps) => {
  return (
    <ol>
      {users.map((el) => (
        <NavLink key={el.id} to={`/users/${el.id}`}>
          {el.title}
        </NavLink>
      ))}
    </ol>
  );
};

export default Users;
