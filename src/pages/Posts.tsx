import React from "react";
import { NavLink } from "react-router-dom";
import { IPost } from "../type";

type PostsProps = {
  posts: IPost[];
};

const Posts = ({ posts }: PostsProps) => {
  return (
    <ul>
      {posts.map((el) => (
        <NavLink key={el.id} to={`/posts/${el.id}`}>
          {el.title}
        </NavLink>
      ))}
    </ul>
  );
};

export default Posts;
