import React from "react";
type HomeProps = {
  isAuth: boolean;
  changeIsAuth: () => void;
};

const Home = ({ isAuth, changeIsAuth }: HomeProps) => {
  return (
    <div className="login">
      <h2>Home</h2>
      <button onClick={changeIsAuth}>{isAuth ? "Logout" : "Login"}</button>
    </div>
  );
};

export default Home;
